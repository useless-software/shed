import psycopg2

class Connection:

  def __init__(self, dbname, host, user, pw):
    self.conn = psycopg2.connect("dbname=" + dbname + " host=" + host + " user=" + user + " password=" + pw)

  def cursor(self):
    if self.conn is None:
      raise Exception("No connection to database!")
    return self.conn.cursor()

  def close(self):
    self.conn.close()


