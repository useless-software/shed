
class Employee:

  def __init__(self, employee_id, name=None, phone_number=None, facilities=[]):
    if not isinstance(employee_id, int):
      raise Exception("Employee id is not a number!")
    self.employee_id = employee_id
    self.name = name
    self.phone_number = phone_number
    self.facilities = facilities

  def add_facility(self, facility_id):
    self.facilities.append(facility_id)

  def remove_facility(self, facility_id):
    self.facilities.remove(facility_id)

  def toJSON(self):
    return json.dumps(self.__dict__)

if __name__ == '__main__':
  e = Employee(1, 'Hi', 'Test', ['c', 'd'])
  print e.facilities
  e.add_facility('e')
  print e.facilities
  e.remove_facility('e')
  print e.facilities
  print "number before: " + e.phone_number
  e.phone_number = 'OK!'
  print "number after:  " + e.phone_number
  print "name before: " + e.name
  e.name = 'Bye'
  print "name after:  " + e.name


