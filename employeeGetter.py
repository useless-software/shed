from getter import Getter
from connection import Connection
from query import Query
from employee import Employee

class EmployeeGetter(Getter):

  def getAllEmployeesForFacility(facility_id):
    command = "SELECT id, name, phone_number FROM employee WHERE %d = ANY (facilities);"
    result = self.query.execute(command, [facility_id])
    rs = result.fetchall
    employees = []
    for x in rs:
      employee = Employee(x[0], x[1], x[2])
      #does the line above this work since constructor should have array of sites at the end?
      employees.append(employee)
    return(employees)