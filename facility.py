import json

class Facility:

  def __init__(self, facility_id, name=None,address=None,phone_number=None):
    if not isinstance(facility_id, int):
      raise Exception("facility id is not a number!")
    self.facility_id = facility_id
    self.name = name
    self.address = address
    self.phone_number = phone_number

  def toJSON(self):
    return json.dumps(self.__dict__)

if __name__ == '__main__':
  s = Facility(1, 'a', 'b','c')
  print s.name
