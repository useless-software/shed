from getter import Getter
from connection import Connection
from query import Query
from facility import Facility

class FacilityGetter(Getter):

  def getAllFacilities(self):
    command = "SELECT id, name, location, phone_number FROM facility"
    result = self.query.execute(command)
    facilities = []
    for x in result.fetchall():
      facility = Facility(*x)
      facilities.append(facility)
    return facilities

  def getFacilityById(self, facility_id):
    command = "SELECT id, name, location, phone_number FROM facility WHERE id = %s"
    result = self.query.execute(command, (facility_id,))
    facilityinfo = result.fetchone()
    facility = Facility(*facilityinfo)

if __name__ == "__main__":
  c = Connection("scheduler", "192.168.1.7", "tester", "test")

  q = Query(c)

  sg = FacilityGetter(q)
  print(sg.getAllFacility())

  print(sg.getFacilityById(1))
