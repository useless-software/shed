from query import Query

class Getter:

  def __init__(self, query):
    if isinstance(query, Query):
      self.query = query
    else:
      raise Exception("Getters must take query!")


  def exportJSON(self, item):
    return json.dumps(item, indent=2, separators=(',', ': '))
