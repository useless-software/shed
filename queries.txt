-- get shifts for some date (given start_time and end_time)
SELECT id, shift_master_id, employee_id, start_date, end_date FROM shift 
WHERE start_date BETWEEN '5/13/2017 00:00' AND '5/14/2017 00:00'
OR end_date BETWEEN '5/13/2017 00:00' AND '5/14/2017 00:00';

-- get shifts for employee for some date (given employee, start_time, end_time)
SELECT id, shift_master_id, employee_id, start_date, end_date FROM shift 
WHERE employee_id = 1 AND
(start_date BETWEEN '5/13/2017 00:00' AND '5/14/2017 00:00'
OR end_date BETWEEN '5/13/2017 00:00' AND '5/14/2017 00:00');

-- get shift for facility for some date (given facility, start_time, end_time)
SELECT id, shift_master_id, employee_id, start_date, end_date FROM shift 
WHERE facility_id = 1 AND
(start_date BETWEEN '5/13/2017 00:00' AND '5/14/2017 00:00'
OR end_date BETWEEN '5/13/2017 00:00' AND '5/14/2017 00:00');

-- get shift master (given facility_id) *note that we use times and not days in this table, must change if we need different shifts on different days
SELECT id, facility_id, start_time, end_time, concurrency FROM shift_master
WHERE facility_id = 1;

-- get facility (get all facility ids)
SELECT id, name, location, phone_number FROM facility;

-- get facility (given facility id)
SELECT id, name, location, phone_number FROM facility
WHERE id = 1;

-- get employee (get all employees)
SELECT id, name, phone_number, facilities FROM employee;

-- get employee (given facility id)
SELECT id, name, phone_number, facilities FROM employee
WHERE 1 = ANY (facilities);

-- assign shift (given employee_id, facility id, start_date, end_date, (optional) shift_master_id)
INSERT INTO shift (employee_id, facility_id, start_date, end_date, shift_master_id)
VALUES (1, 1, '5/14/2017 08:00', '5/14/2017 12:30', null);

-- unassign shift (given shift_id, called_off_reason)
UPDATE shift SET called_off_reason = 'hit by bus'
WHERE id = 7;


