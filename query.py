from connection import Connection

class Query:

  def __init__(self, conn):
    self.conn = conn

  def execute(self, query, arg_tuple = None):
    cur = self.conn.cursor()
    if arg_tuple is not None:
      cur.execute(query, arg_tuple)
    else:
      cur.execute(query)
    return cur

  def commit(self):
    self.conn.conn.commit()

if __name__ == "__main__":
  c = Connection("scheduler", "192.168.1.7", "tester", "test")

  q = Query(c)

  cur = q.execute("INSERT INTO employee (name, phone_number, sites) VALUES (%s, %s, %s)", ("Jorf Masterson", "135-444-0284", "{1,2}"))
  q.commit()
  cur = q.execute("SELECT * FROM employee WHERE id = %s", (15,))

  rs = cur.fetchall()
  
  for x in rs:
    print x
  cur.close()
  q.conn.close()

