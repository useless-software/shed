from flask import Flask, request, send_from_directory
from xdomain import *
from connection import Connection
from query import Query
from facility import Facility
from facilityGetter import FacilityGetter
from shiftGetter import ShiftGetter
from shiftSetter import ShiftSetter
import json
import os

c = Connection("scheduler", "192.168.1.5", "tester", "test")

q = Query(c)

shed = Flask(__name__, static_url_path='')

def exportJSONList(resp):
  jsonlist = [x.toJSON() for x in resp]
  return json.dumps(jsonlist, indent=2, separators=(',', ': '))


@shed.route("/shed/facilities", methods=['GET', 'OPTIONS'])
@crossdomain(origin='*')
def getAllFacilities():
  sg = FacilityGetter(q)
  facs = sg.getAllFacilities()
  return exportJSONList(facs)

@shed.route("/shed/shiftmaster/facility/<facility_id>", methods=["GET"])
@crossdomain(origin='*')
def getShiftMasterById(facility_id):
  sg = ShiftGetter(q)
  shift_masters = sg.getShiftMaster(facility_id)
  return exportJSONList(shift_masters)

@shed.route("/shed/shifts/facility/<facility_id>/start/<start_time>/end/<end_time>", methods=["GET"])
@crossdomain(origin='*')
def getShiftsByFacility(facility_id, start_time, end_time):
  sg = ShiftGetter(q)
  start_time = start_time.replace("+", " ")
  end_time = end_time.replace("+", " ")
  shifts = sg.getExistingShiftsByFacility(facility_id, start_time, end_time)
  return exportJSONList(shifts)

@shed.route("/shed/shifts/employee/<employee_id>/start/<start_time>/end/<end_time>", methods=["GET"])
@crossdomain(origin='*')
def getShiftsByEmployee(employee_id, start_time, end_time):
  sg = ShiftGetter(q)
  start_time = start_time.replace("+", " ")
  end_time = end_time.replace("+", " ")
  shifts = sg.getExistingShiftsByEmployee(employee_id, start_time, end_time)
  return exportJSONList(shifts)

@shed.route("/shed/shifts/shiftSetter", methods=["PUT"])
@crossdomain(origin='*')
def setShift():
  ss = ShiftSetter(q)
  return ss.assignShift(request.form['employee_id'], request.form['facility_id'], request.form['start_date'], request.form['end_date'], request.form['shift_master_id'])

@shed.route("/shed/shifts/shiftSetter", methods=["POST"])
@crossdomain(origin='*')
def unsetShift():
  ss = ShiftSetter(q)
  return ss.unassignShift(request.form['shift_id'], request.form['called_off_reason'])

@shed.route("/js/<filename>", methods=["GET"])
@crossdomain(origin='*')
def getjs(filename):
  realpath = os.path.dirname(os.path.realpath(__file__))
  realfile = realpath + "/js/" + filename
  with open(realfile) as f:
    data = f.read()
  return data

@shed.route("/examplepage", methods=["GET"])
@crossdomain(origin='*')
def showpage():
  return send_from_directory('', 'display-test.html')

if __name__ == '__main__':
  shed.run("0.0.0.0")

