import json

class Shift:

  def __init__(self, shift_id, employee_id, facility_id, start_date, end_date, called_off_reason=None, shift_master_id=None):
    if not isinstance(shift_id, int):
      raise Exception("Shift id is not a number!")
    self.shift_id = shift_id
    self.shift_master_id = shift_master_id
    self.employee_id = employee_id
    self.facility_id = facility_id
    self.start_date = start_date
    self.end_date = end_date
    self.called_off_reason = called_off_reason

  def toJSON(self):
    self.start_date = str(self.start_date)
    self.end_date = str(self.end_date)
    return json.dumps(self.__dict__)
