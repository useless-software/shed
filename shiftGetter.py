from getter import Getter
from connection import Connection
from query import Query
from shift import Shift

class ShiftGetter(Getter):

  def getExistingShiftsByFacility(self, facility_id, start_date, end_date):
    command = "SELECT id, employee_id, facility_id, start_date, end_date FROM shift WHERE facility_id = %s AND ((start_date BETWEEN %s AND %s) OR (end_date BETWEEN %s AND %s))"
    result = self.query.execute(command, [facility_id, start_date, end_date, start_date, end_date])
    rs = result.fetchall()
    shifts = []
    for x in rs:
      shift = Shift(x[0], x[1], x[2], x[3], x[4])
      shifts.append(shift)
    return shifts

  def getExistingShiftsByEmployee(self, employee_id, start_date, end_date):
    command = "SELECT id, employee_id, facility_id, start_date, end_date FROM shift WHERE employee_id = %s AND ((start_date BETWEEN %s AND %s) OR (end_date BETWEEN %s AND %s));"
    result = self.query.execute(command, [employee_id, start_date, end_date, start_date, end_date])
    rs = result.fetchall()
    shifts = []
    for x in rs:
      shift = Shift(x[0], x[1], x[2], x[3], x[4])
      shifts.append(shift)
    return shifts
    
  def getShiftMaster(self, facility_id):
    command = "SELECT start_time, end_time, concurrency FROM shift_master WHERE facility_id = %s"
    result = self.query.execute(command, (facility_id,))
    rs = result.fetchall()
    shifts = []
    for row in rs:
      shift = Shift(0, 0, facility_id, row[0], row[1])
      shifts.append(shift)
    return shifts
