from getter import Getter
from employee import Employee
from connection import Connection
from query import Query

class ShiftSetter(Getter):

  def assignShift(self, employee_id, facility_id, start_date, end_date, shift_master_id=None):
    if shift_master_id is None:
      command = "INSERT INTO shift (employee_id, facility_id, start_date, end_date) VALUES (%s, %s, %s, %s);"
      result = self.query.execute(command, [employee_id, facility_id, start_date, end_date])
    else:
      command = "INSERT INTO shift (employee_id, facility_id, start_date, end_date, shift_master_id) VALUES (%s, %s, %s, %s, %s);"
      self.query.execute(command, [employee_id, facility_id, start_date, end_date, shift_master_id])
      self.query.commit()
    #error handling?
    return("Successfully added shift for " + employee_id)

  def unassignShift(self, shift_id, called_off_reason):
    command = "UPDATE shift SET called_off_reason = %s WHERE id = %s;"
    result = self.query.execute(command, [called_off_reason, shift_id])
    self.query.commit()
    #doesn't return anything so if no errors should we return anything?
    return("Successfully removed shift " + shift_id)